## Exercises Chapter 1

### Exercise 1

#### Question

What is the function of the secondary memory in a computer.

#### Answer

Store information for the long term, even beyond a power cycle.
